import HttpClient from './httpClient';

class PostService {
  static async getPosts(url) {
    const response = await HttpClient.get(url);
    console.log(response);
    return response;
  }
}

export default PostService;
