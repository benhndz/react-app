import React from 'react';
import { useSelector } from 'react-redux';
import styled, { ThemeProvider } from 'styled-components';
import { darkTheme, defaultTheme } from '../ui/vars';
import { GlobalStyle } from '../ui/vars';
import MainMenu from './MainMenu';

const Main = styled.main`
  width: 1080px;
  height: 100%;
  padding-left: 360px;
  overflow: hidden;
`;

const MainWrapper = (props) => {
  const theme = useSelector((state) => state.theme);

  return (
    <ThemeProvider theme={theme === 'dark' ? darkTheme : defaultTheme}>
      <GlobalStyle />
      <div
        style={{
          background:
            theme === 'dark'
              ? defaultTheme.primaryColor
              : darkTheme.primaryColor,
          width: '100vw',
          height: '100vh',
          overflow: 'hidden',
        }}
      >
        <MainMenu />
        <Main>{props.children}</Main>
      </div>
    </ThemeProvider>
  );
};

export default MainWrapper;
