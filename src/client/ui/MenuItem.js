import styled from 'styled-components';
import { motion } from 'framer-motion';

const MenuWItemWrapper = styled(motion.div)`
  width: calc(100% - 4px);
  height: 70px;
  line-height: 70px;
  padding-left: 30px;
  color: ${(props) => props.theme.textColorOnPrimary};
  /* border-bottom: 1px solid ${(props) => props.theme.textColorOnPrimary}; */
  cursor: pointer;
  box-shadow: ${(props) => props.theme.primaryHoverColor} 1px 1px 0,
    ${(props) => props.theme.primaryShadowColor} 2px 2px 0,
    ${(props) => props.theme.primaryShadowColor} 3px 3px 0,
    ${(props) => props.theme.primaryShadowColor} 4px 4px 0;

  &:hover {
    background-color: ${(props) => props.theme.primaryHoverColor};
    color: ${(props) => props.theme.textColorOnPrimary};
  }
`;

const MenuItem = (props) => {
  return (
    <MenuWItemWrapper
      {...props}
      initial={{ opacity: 0 }}
      animate={{
        opacity: 1,
        transition: {
          type: 'spring',
          delay: 1.5 + props.index / 10,
        },
      }}
    >
      {props.children}
    </MenuWItemWrapper>
  );
};

export default MenuItem;
