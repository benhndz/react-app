export const blue = {
  100: '#036fb4',
  200: '#0468a9',
  300: '#005C94',
  400: '#004C7A',
  500: '#00436B',
};

export const green = {
  100: '#529e66',
  200: '#367b48',
  300: '#276738',
};

export const yellow = {
  100: '#e1c542',
  200: '#cab23f',
  300: '#b49e35',
};

export const red = {
  100: '#d0454c',
  200: '#b54248',
  300: '#95353a',
};

export const neutral = {
  100: '#ffffff',
  200: '#f4f5f7',
  300: '#e1e1e1',
  400: '#737581',
  500: '#4a4b53',
  600: '#000000',
};
