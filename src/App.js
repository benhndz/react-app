import React, { Suspense, lazy } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import MainWrapper from './client/modules/MainWrapper';

const HomeRoute = lazy(() =>
  import('./client/pages/Home' /* webpackChunkName: "home" */)
);

const FallBack = () => <div className="contentFallback">Loading</div>;

const App = () => {
  return (
    <Provider store={store}>
      <MainWrapper>
        <Router>
          <Suspense fallback={<FallBack />}>
            <Route exact path="/" component={HomeRoute} />
          </Suspense>
        </Router>
      </MainWrapper>
    </Provider>
  );
};

export default App;
