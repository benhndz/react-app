import { HTTP_STATUS } from '../constants';
import { getPosts } from '../thunks/post';

export const postsReducer = {
  //case reducer: posts/add
  add: (state, action) => {
    state.push(action.payload);
  },

  //case reducer: posts/remove
  remove: (state, action) => {
    const index = state.findIndex((post) => post.id === action.payload);
    state.splice(index, 1);
  },
};

export const postsExtraReducer = {
  [getPosts.pending]: (state) => {
    state.loading = HTTP_STATUS.PENDING;
    // state.status = action.meta.requestStatus
    // state.error = {}
  },
  [getPosts.fulfilled]: (state, { payload }) => {
    state.data = payload;
    state.loading = HTTP_STATUS.FULFILLED;
    // state.status = action.meta.requestStatus
    // state.error = {}
  },
  [getPosts.rejected]: (state, { error }) => {
    state.loading = HTTP_STATUS.REJECTED;
    state.error = error.message;
    // state.status = action.meta.requestStatus
    // state.error = action.error
  },
};
