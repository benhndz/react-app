import { createSlice } from '@reduxjs/toolkit';
import { postsReducer, postsExtraReducer } from '../reducers/posts';

const namespace = 'posts';

const initialState = {
  loading: null,
  data: null,
  error: null,
};

const postsSlice = createSlice({
  name: namespace,
  initialState: initialState,
  reducers: postsReducer,
  extraReducers: postsExtraReducer,
});

export default postsSlice;
