import React from 'react';
import styled from 'styled-components';

const PostListWrapper = styled.ul`
  padding: 0;
  margin: 0;
  list-style: none;
  li {
    margin-bottom: 20px;
    padding: 20px;
    border: 1px solid black;
    border-radius: 6px;
  }
  li h3 {
    margin: 0 0 10px;
    font-size: 16px;
  }
  li span {
    display: inline-block;
    font-size: 12px;
    font-weight: 600;
  }
  li p {
    margin: 0 0 10px;
    font-size: 12px;
  }
  .metadata {
    margin-bottom: 10px;
  }
`;

const PostList = () => {
  return (
    <PostListWrapper>
      <li>
        <article>
          <h3>Title</h3>
          <div className="metadata">
            <span>Author: Ben</span>
            <span>05/15/2021</span>
          </div>
          <div className="textContent">
            <p>
              "Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis et quasi architecto beatae vitae
              dicta sunt explicabo.
            </p>
          </div>
          <span>Read More</span>
        </article>
      </li>
      <li>
        <article>
          <h3>Title</h3>
          <div className="metadata">
            <span>Author: Ben</span>
            <span>05/15/2021</span>
          </div>
          <div className="textContent">
            <p>
              "Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis et quasi architecto beatae vitae
              dicta sunt explicabo.
            </p>
          </div>
          <span>Read More</span>
        </article>
      </li>
      <li>
        <article>
          <h3>Title</h3>
          <div className="metadata">
            <span>Author: Ben</span>
            <span>05/15/2021</span>
          </div>
          <div className="textContent">
            <p>
              "Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis et quasi architecto beatae vitae
              dicta sunt explicabo.
            </p>
          </div>
          <span>Read More</span>
        </article>
      </li>
    </PostListWrapper>
  );
};

export default PostList;
