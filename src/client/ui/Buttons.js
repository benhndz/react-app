import styled from 'styled-components';
import { typeScale, primaryFont } from './vars';
import { applyStyleModifiers } from 'styled-components-modifiers';
import { motion } from 'framer-motion';

/**
 * Buttons Modifiers
 */

const BUTTON_MODIFIERS = {
  small: () => `
    font-size: ${typeScale.helperText};
    padding: 8px;
  `,
  large: () => `
    font-size: ${typeScale.header5};
    padding: 16px 24px;
  `,
  warning: ({ theme }) => `
    background-color: ${theme.status.warningColor};
    color: ${(props) => theme.textColorInverted};
    
    &:hover, &:focus {
      background-color: ${theme.status.warningColorHover};
      outline: 3px solid ${theme.status.warningColorHover};
      outline-offset: 2px;
    }

    &:active {
      background-color: ${theme.status.warningColorActive};
    }
  `,
  error: ({ theme }) => `
    background-color: ${theme.status.errorColor};
    color: ${(props) => props.theme.textColorInverted};

    &:hover {
      background-color: ${theme.status.errorColorHover};
    }

    &:active {
      background-color: ${theme.status.errorColorActive};
    }
  `,
  success: ({ theme }) => `
    background-color: ${theme.status.successColor};
    color: ${theme.textColorInverted};

    &:hover {
      background-color: ${theme.status.successColorHover};
    }

    &:active {
      background-color: ${theme.status.successColorActive};
    }
  `,
};

/**
 * Buttons styles
 */

const ButtonStyle = styled(motion.button)`
  padding: 6px 12px;
  font-family: ${primaryFont};
  font-size: ${typeScale.paragraph};
  border-radius: 2px;
  min-width: 100px;
  cursor: pointer;
  transition: background-color 0.2s linear, color 0.2s linear;

  &:hover {
    background-color: ${(props) => props.theme.primaryHoverColor};
    color: ${(props) => props.theme.textColorOnPrimary};
  }

  &:focus {
    outline: 3px solid ${(props) => props.theme.primaryHoverColor};
    outline-offset: 2px;
  }

  &:active {
    background-color: ${(props) => props.theme.primaryActiveColor};
    border-color: ${(props) => props.theme.primaryActiveColor};
    color: ${(props) => props.theme.textColorOnPrimary};
  }
`;

/**
 * Buttons animation
 */

const containerVariants = {
  hidden: {
    opacity: 0,
    x: '-10px',
  },
  visible: {
    opacity: 1,
    x: 0,
    transition: { type: 'spring', delay: 0.5 },
  },
};

export const Button = (props) => {
  return (
    <ButtonStyle
      {...props}
      whileHover={{ scale: 1.1 }}
      variants={containerVariants}
      initial="hidden"
      animate="visible"
    >
      {props.children}
    </ButtonStyle>
  );
};

/**
 * Buttons types
 */

export const PrimaryButton = styled(Button)`
  background-color: ${(props) => props.theme.primaryColor};
  border: 2px solid transparent;
  color: ${(props) => props.theme.textColorOnPrimary};

  &:disabled {
    background-color: ${(props) => props.theme.disabled};
    color: ${(props) => props.theme.textOnDisabled};
    cursor: not-allowed;
  }

  ${applyStyleModifiers(BUTTON_MODIFIERS)}
`;

export const SecondaryButton = styled(Button)`
  border: 2px solid ${(props) => props.theme.primaryColor};
  color: ${(props) => props.theme.primaryColor};
  background: none;

  &:disabled {
    background: none;
    border: 2px solid ${(props) => props.theme.disabled};
    color: ${(props) => props.theme.disabled};
    cursor: not-allowed;
  }
  ${applyStyleModifiers(BUTTON_MODIFIERS)}
`;

export const TertiaryButton = styled(Button)`
  border: 2px solid transparent;
  color: ${(props) => props.theme.primaryColor};
  background: none;

  &:disabled {
    color: ${(props) => props.theme.disabled};
    cursor: not-allowed;
  }
  ${applyStyleModifiers(BUTTON_MODIFIERS)}
`;

export default PrimaryButton;
