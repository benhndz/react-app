import { createAsyncThunk } from '@reduxjs/toolkit';
import PostService from '../api/postService';

const namespace = 'posts';

export const getPosts = createAsyncThunk(`${namespace}/getPosts`, async () => {
  const response = await PostService.getPosts(namespace);
  return response;
});

// Cancel request before execution
// dispatch another action

// export const getPosts = createAsyncThunk('posts/getPosts', async ({limit}, {dispatch, getState}) => {
//   const { posts } = getState()
//   const response = await PostService.getPosts();
//   dispatch(action);
//   return response;
// },
// {
//   condition: (obj, {getState}) => {
//     const { users } = getState()
//     if(users.loading === HTTP_STATUS.PENDING) {
//       return false;
//     }
//   }
// });
