import styled from 'styled-components';
import { motion } from 'framer-motion';

const containerVariants = {
  hidden: {
    opacity: 0,
    x: '-100%',
  },
  visible: {
    opacity: 1,
    x: 0,
    transition: {
      type: 'spring',
      delay: 0.5,
      when: 'beforeChildren',
    },
  },
};

const MenuWrapper = styled(motion.div)`
  position: fixed;
  top: 0;
  left: 0;
  width: 360px;
  height: 100%;
  background-color: ${(props) => props.theme.primaryColor};
`;

const Menu = (props) => {
  return (
    <MenuWrapper
      variants={containerVariants}
      initial="hidden"
      animate="visible"
    >
      {props.children}
    </MenuWrapper>
  );
};

export default Menu;
