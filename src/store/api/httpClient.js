import { API_URL } from '../constants';

class HttpClient {
  instance = null;

  constructor() {
    if (!this.instance) {
      this.instance = this;
    }
    return this.instance;
  }

  request(method, url, data, options) {
    return fetch(API_URL + url, {
      method,
      mode: 'cors',
      body: data,
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.status === 401) {
          throw new Error(response.message);
        }
        return response;
      })
      .catch((error) => {
        return Promise.reject(error);
      });
  }

  get(url, options) {
    return this.request('GET', url, null, options);
  }

  post(url, data, options) {
    return this.request('POST', url, data, options);
  }

  put(url, data, options) {
    return this.request('PUT', url, data, options);
  }

  del(url, data, options) {
    return this.request('DELETE', url, data, options);
  }
}
const httpClient = new HttpClient();
export default httpClient;
