import React, { useEffect } from 'react';
import styled from 'styled-components';
import PostList from '../modules/PostList';
import { useSelector, useDispatch } from 'react-redux';
import { getPosts } from '../../store/thunks/post';
import { HTTP_STATUS } from '../../store/constants';

const PostListWrapper = styled.section`
  padding: 40px;
  h2 {
    margin: 0 0 20px;
  }
`;

const Home = () => {
  const postsState = useSelector((state) => state.posts);
  const dispatch = useDispatch();

  useEffect(() => {
    const promise = dispatch(getPosts());
    // cancel thunk while running
    return () => {
      promise.abort();
    };
  }, [dispatch]);

  return (
    <PostListWrapper id="postList">
      <h2>Latest Posts</h2>
      {postsState.loading === HTTP_STATUS.PENDING && <div>LOADING...</div>}
      {postsState.loading === HTTP_STATUS.REJECTED && (
        <div>{postsState.error}</div>
      )}
      {postsState.loading === HTTP_STATUS.FULFILLED && <PostList />}
    </PostListWrapper>
  );
};

export default Home;
