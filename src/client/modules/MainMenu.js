import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import Menu from '../ui/Menu';
import MenuItem from '../ui/MenuItem';
import { setTheme } from '../../store/slices/theme';

const Title = styled.h1`
  display: inline-block;
  margin-left: 20px;
  padding: 10px;
  font-size: 32px;
  color: ${(props) => props.theme.textColorOnPrimary};
  border: 2px solid ${(props) => props.theme.textColorOnPrimary};
`;

const MenuItems = [
  {
    title: 'Login',
    action: '',
  },
  {
    title: 'Register',
    action: '',
  },
];

const MainMenu = () => {
  const dispatch = useDispatch();

  function handleTheme(theme) {
    dispatch(setTheme(theme));
  }

  return (
    <Menu>
      <Title>React App</Title>
      <nav role="navigation">
        <MenuItem
          style={{
            display: 'inline-block',
            width: 'calc(50% - 2px)',
            margin: '0',
          }}
          onClick={() => handleTheme('dark')}
        >
          Dark theme
        </MenuItem>
        <MenuItem
          style={{
            display: 'inline-block',
            width: 'calc(50% - 2px)',
            margin: '0',
          }}
          onClick={() => handleTheme('default')}
        >
          Default theme
        </MenuItem>
        {MenuItems.map((item, index) => {
          return (
            <MenuItem key={item.title} index={index}>
              {item.title}
            </MenuItem>
          );
        })}
      </nav>
    </Menu>
  );
};

export default MainMenu;
