import postsSlice from './slices/posts';
import themeSlice from './slices/theme';
import { configureStore } from '@reduxjs/toolkit';

const allReducers = {
  theme: themeSlice,
  posts: postsSlice.reducer,
};

const store = configureStore({
  reducer: allReducers,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: (getDefaultMiddleware) => [...getDefaultMiddleware()],
});

export default store;
