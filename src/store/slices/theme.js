import { createSlice } from '@reduxjs/toolkit';
import themeReducer from '../reducers/theme';

const initialState = 'default';

const themeSlice = createSlice({
  name: 'theme',
  initialState: initialState,
  reducers: themeReducer,
});

export const { setTheme } = themeSlice.actions;
export default themeSlice.reducer;
